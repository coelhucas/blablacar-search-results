import { QueryClient, QueryClientProvider } from "react-query";
import SearchResults from "./containers/search_results";

const queryClient = new QueryClient();

export default function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <SearchResults />
    </QueryClientProvider>
  );
}
