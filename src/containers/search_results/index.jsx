import { useSearchResults } from "../../hooks/useSearchResults";

import SearchResultList from "../../components/search_result_list";

const SearchResults = () => {
  const {
      isLoading,
      error,
      data,
      fetchNextPage,
      hasNextPage
    } = useSearchResults();

  if (isLoading) return <div>Loading</div>;

  if (error) return <div>{error.message}</div>;
  return (
    <SearchResultList
      onClickShowMore={fetchNextPage}
      hasNextPage={hasNextPage}
      results={data}
    />
  );
};

export default SearchResults;
