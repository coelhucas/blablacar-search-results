import { render } from "@testing-library/react"
import { QueryClient, QueryClientProvider } from "react-query"
import SearchResultList from "."
import { useTripData } from "../../hooks/useTripData"

const mockProps = {
  results: { "pages": [{ "link": "https://www.blablacar.co.uk/search?fc=48.8566,2.3522&tc=45.764043,4.835659&fn=Paris&tn=Lyon&db=2021-10-18&de=2021-10-18", "search_info": { "count": 9, "full_trip_count": 2 }, "trips": [{ "link": "https://www.blablacar.co.uk/trip?source=CARPOOLING&id=2305307859-fr-fr", "waypoints": [{ "date_time": "2021-10-18T17:30:00", "place": { "city": "Paris", "address": "48 bis Bd de Bercy, Paris", "latitude": 48.839143, "longitude": 2.382741, "country_code": "FR" } }, { "date_time": "2021-10-18T22:30:00", "place": { "city": "Lyon", "address": "Lyon Part Dieu, 5 Pl. Charles Béraudier, Lyon", "latitude": 45.760547, "longitude": 4.861117, "country_code": "FR" } }], "price": { "amount": "26.00", "currency": "EUR" }, "distance_in_meters": 465189, "duration_in_seconds": 18000 }, { "link": "https://www.blablacar.co.uk/trip?source=CARPOOLING&id=2305114164-paris-lyon", "waypoints": [{ "date_time": "2021-10-18T18:00:00", "place": { "city": "Paris", "address": "Belleville, Paris", "latitude": 48.872029, "longitude": 2.377386, "country_code": "FR" } }, { "date_time": "2021-10-18T23:50:00", "place": { "city": "Lyon", "address": "44 Rue Raoul Servant, Lyon", "latitude": 45.746408, "longitude": 4.841385, "country_code": "FR" } }], "price": { "amount": "26.00", "currency": "EUR" }, "distance_in_meters": 514451, "duration_in_seconds": 21000 }, { "link": "https://www.blablacar.co.uk/trip?source=CARPOOLING&id=2305105054-fr-fr", "waypoints": [{ "date_time": "2021-10-18T18:00:00", "place": { "city": "Massy", "address": "20 Av. Carnot, Massy", "latitude": 48.725743, "longitude": 2.261212, "country_code": "FR" } }, { "date_time": "2021-10-18T22:40:00", "place": { "city": "Lyon", "address": "Lyon, Lyon", "latitude": 45.748668, "longitude": 4.826, "country_code": "FR" } }], "price": { "amount": "30.00", "currency": "EUR" }, "vehicle": { "make": "NISSAN", "model": "JUKE" }, "distance_in_meters": 449563, "duration_in_seconds": 16800 }, { "link": "https://www.blablacar.co.uk/trip?source=CARPOOLING&id=2304142434-saint-denis-lyon", "waypoints": [{ "date_time": "2021-10-18T18:40:00", "place": { "city": "Saint-Denis", "address": "17 R. Danielle Casanova, Saint-Denis", "latitude": 48.930632, "longitude": 2.361332, "country_code": "FR" } }, { "date_time": "2021-10-18T23:40:00", "place": { "city": "Lyon", "address": "14 Av. Berthelot, Lyon", "latitude": 45.7469429, "longitude": 4.835185, "country_code": "FR" } }], "price": { "amount": "26.00", "currency": "EUR" }, "vehicle": { "make": "MERCEDES", "model": "CLASSE A" }, "distance_in_meters": 475822, "duration_in_seconds": 18000 }, { "link": "https://www.blablacar.co.uk/trip?source=CARPOOLING&id=2301407868-gennevilliers-lyon", "waypoints": [{ "date_time": "2021-10-18T18:40:00", "place": { "city": "Gennevilliers", "address": "Gare de Gennevilliers, Gennevilliers", "latitude": 48.9334, "longitude": 2.307096, "country_code": "FR" } }, { "date_time": "2021-10-18T23:50:00", "place": { "city": "Lyon", "address": "84 Quai Perrache, Lyon", "latitude": 45.734521, "longitude": 4.818071, "country_code": "FR" } }], "price": { "amount": "43.00", "currency": "EUR" }, "vehicle": { "make": "MERCEDES", "model": "W140" }, "distance_in_meters": 477338, "duration_in_seconds": 18600 }], "next_cursor": "cGFnZT0x" }], "pageParams": [null] }
}

const mockTripData = {"links":{"_self":"https://api.blablacar.com/api/v2/trips/2304142434-saint-denis-lyon","_front":"https://www.blablacar.co.uk/trip/carpooling/2304142434-saint-denis-lyon/"},"departure_date":"18/10/2021 18:40:00","departure_date_iso8601":"2021-10-18T18:40:00+01:00","is_passed":false,"departure_place":{"city_name":"Saint-Denis","address":"Hôpital Casanova","latitude":48.930632,"longitude":2.361332,"country_code":"FR"},"arrival_place":{"city_name":"Lyon","address":"14 Avenue Berthelot, 69007 Lyon, France","latitude":45.746942,"longitude":4.835185,"country_code":"FR","meeting_point_id":342313},"arrival_meeting_point":{"id":342313,"place_id":"eyJpIjoiMzQyMzEzIiwicCI6NCwidiI6MSwidCI6W119","name":"Institut des Sciences de l'Homme","full_name":"Institut des Sciences de l'Homme, Lyon","city_name":"Lyon","address":"14 Avenue Berthelot, 69007 Lyon, France","latitude":45.746942,"longitude":4.835185,"country_code":"FR","tags":["university"]},"price":{"value":22,"currency":"GBP","symbol":"£","string_value":"£22.00","price_color":"BLACK"},"price_with_commission":{"value":22,"currency":"GBP","symbol":"£","string_value":"£22.00","price_color":"BLACK"},"price_without_commission":{"value":16,"currency":"GBP","symbol":"£","string_value":"£16.00","price_color":"BLACK"},"commission":{"value":5,"currency":"GBP","symbol":"£","string_value":"£5.00"},"seats_left":2,"seats":2,"seats_count_origin":2,"duration":{"value":18000,"unity":"s"},"distance":{"value":475,"unity":"km"},"permanent_id":"2304142434-saint-denis-lyon","main_permanent_id":"2304142434-saint-denis-aix-en-provence","corridoring_id":"2304142434-saint-denis-lyon","trip_offer_encrypted_id":"1ec2f056-daf9-6cf5-b443-45860f980961","comment":"Bonjour,\n\nDépart devant hôpital Casanova entre 18h40 et 18h45.","car":{"id":"T1EKiyegwTLJnnEJ-HBs7Q","model":"CLASSE A","make":"MERCEDES","color":"Dark grey","color_hexa":"727272","comfort":"Normal","comfort_nb_star":2,"number_of_seat":4,"category":"_UE_BERLINE","picture":"https://cdn.blablacar.com/comuto3/images/rebranding/icons/vehicles/car-saloon.svg"},"viaggio_rosa":false,"is_comfort":true,"freeway":true,"stop_overs":[{"city_name":"Saint-Denis","address":"Hôpital Casanova","latitude":48.930632,"longitude":2.361332,"country_code":"FR","departure_place":true},{"city_name":"Mâcon","latitude":46.282944,"longitude":4.792796,"country_code":"FR"},{"city_name":"Lyon","latitude":45.746942,"longitude":4.835185,"country_code":"FR","arrival_place":true},{"city_name":"Aix-en-Provence","address":"Pasino GRAND","latitude":43.523155,"longitude":5.435842,"country_code":"FR"}],"bucketing_eligible":false,"booking_mode":"manual","booking_type":"online","is_booking_allowed":true,"view_count":166,"cross_border_alert":false,"trip_plan":[{"date":"18/10/2021 18:40:00","city":"Saint-Denis","address":"Hôpital Casanova","latitude":48.930632,"longitude":2.361332,"country_code":"FR","meeting_point_id":0,"vinci_shuttle_enabled":false},{"date":"18/10/2021 22:50:00","city":"Mâcon","address":"Péage de Mâcon Sud, 71000 Mâcon","latitude":46.282944,"longitude":4.792796,"country_code":"FR","meeting_point_id":0,"vinci_shuttle_enabled":false},{"date":"18/10/2021 23:40:00","city":"Lyon","address":"14 Avenue Berthelot, 69007 Lyon, France","latitude":45.746942,"longitude":4.835185,"country_code":"FR","meeting_point_id":342313,"vinci_shuttle_enabled":false},{"date":"19/10/2021 02:50:00","city":"Aix-en-Provence","address":"Pasino GRAND","latitude":43.523155,"longitude":5.435842,"country_code":"FR","meeting_point_id":0,"vinci_shuttle_enabled":false}],"messaging_status":"private","passengers":[],"display_contact":true,"vehicle_pictures":[],"can_report":false,"is_archived":false,"is_too_late_to_book":false,"is_booking_refused_by_driver":false,"has_already_booked_another_ride":false,"is_wrong_gender":false};

jest.mock("../../hooks/useTripData", () => ({
  useTripData: jest.fn()
}))

it('should successfully render five search results with show more button', () => {
  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
      }
    }
  });

  useTripData.mockImplementation(() => ({
    data: mockTripData
  }));

  const { getByText, getByRole, getAllByText } = render(
    <QueryClientProvider client={queryClient}>
      <SearchResultList
        hasNextPage
        onClickShowMore={() => {}}
        {...mockProps}
      />
    </QueryClientProvider>
  );
 
  /* Since we're saying that it hasNextPage, it should appear */
  expect(getByRole('button', { name: /show more/i })).toBeDefined();

  /* Total results from mockProps */
  expect(getByText(/showing 5 of 9 results found/i)).toBeDefined();

  /* Arbitrary: since this data is mocked, we only want to guarantee that it shows up correctly */
  expect(getAllByText(/saint-denis/i)).toHaveLength(5);
})