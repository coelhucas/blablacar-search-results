import React from "react";
import SearchResult from "../search_result";
import "./styles.css";

const getTripID = (url) => {
  const params = new URLSearchParams(url);
  return params.get("id") ?? null;
};

const SearchResultList = ({ results, hasNextPage, onClickShowMore }) => {
  const { pages = [] } = results;
  const [firstPage] = pages;
  const resultsString = firstPage?.search_info?.count > 1 ? "results" : results;
  const displayedResults = pages.reduce((acc, page) => acc + page.trips.length, 0);
  const totalHiddenResults = pages.reduce((acc, page) => acc + page.search_info.full_trip_count, 0);

  return (
    <article>
      {firstPage ? (
        <h1>
          Showing {displayedResults} of {firstPage.search_info.count} {resultsString} found.{' '}
          {totalHiddenResults > 0 &&
            <>
              {totalHiddenResults} hidden results from full trips.
            </>
          }
        </h1>
      ) : null}
      {pages?.map((page) => (
        <ul className="page-list" key={page.link}>
          {page.trips.map((trip) => (
            <SearchResult key={trip.link} tripID={getTripID(trip.link)} />
          ))}
        </ul>
      ))}
      {hasNextPage && <button onClick={onClickShowMore} aria-label="show more">Show more</button>}
    </article>
  );
};

export default SearchResultList;
