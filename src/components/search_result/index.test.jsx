import { render } from "@testing-library/react"
import { QueryClient, QueryClientProvider } from "react-query"
import SearchResult from "."

import { useTripData } from "../../hooks/useTripData"

const mockTripData = {"links":{"_self":"https://api.blablacar.com/api/v2/trips/2304142434-saint-denis-lyon","_front":"https://www.blablacar.co.uk/trip/carpooling/2304142434-saint-denis-lyon/"},"departure_date":"18/10/2021 18:40:00","departure_date_iso8601":"2021-10-18T18:40:00+01:00","is_passed":false,"departure_place":{"city_name":"Saint-Denis","address":"Hôpital Casanova","latitude":48.930632,"longitude":2.361332,"country_code":"FR"},"arrival_place":{"city_name":"Lyon","address":"14 Avenue Berthelot, 69007 Lyon, France","latitude":45.746942,"longitude":4.835185,"country_code":"FR","meeting_point_id":342313},"arrival_meeting_point":{"id":342313,"place_id":"eyJpIjoiMzQyMzEzIiwicCI6NCwidiI6MSwidCI6W119","name":"Institut des Sciences de l'Homme","full_name":"Institut des Sciences de l'Homme, Lyon","city_name":"Lyon","address":"14 Avenue Berthelot, 69007 Lyon, France","latitude":45.746942,"longitude":4.835185,"country_code":"FR","tags":["university"]},"price":{"value":22,"currency":"GBP","symbol":"£","string_value":"£22.00","price_color":"BLACK"},"price_with_commission":{"value":22,"currency":"GBP","symbol":"£","string_value":"£22.00","price_color":"BLACK"},"price_without_commission":{"value":16,"currency":"GBP","symbol":"£","string_value":"£16.00","price_color":"BLACK"},"commission":{"value":5,"currency":"GBP","symbol":"£","string_value":"£5.00"},"seats_left":2,"seats":2,"seats_count_origin":2,"duration":{"value":18000,"unity":"s"},"distance":{"value":475,"unity":"km"},"permanent_id":"2304142434-saint-denis-lyon","main_permanent_id":"2304142434-saint-denis-aix-en-provence","corridoring_id":"2304142434-saint-denis-lyon","trip_offer_encrypted_id":"1ec2f056-daf9-6cf5-b443-45860f980961","comment":"Bonjour,\n\nDépart devant hôpital Casanova entre 18h40 et 18h45.","car":{"id":"T1EKiyegwTLJnnEJ-HBs7Q","model":"CLASSE A","make":"MERCEDES","color":"Dark grey","color_hexa":"727272","comfort":"Normal","comfort_nb_star":2,"number_of_seat":4,"category":"_UE_BERLINE","picture":"https://cdn.blablacar.com/comuto3/images/rebranding/icons/vehicles/car-saloon.svg"},"viaggio_rosa":false,"is_comfort":true,"freeway":true,"stop_overs":[{"city_name":"Saint-Denis","address":"Hôpital Casanova","latitude":48.930632,"longitude":2.361332,"country_code":"FR","departure_place":true},{"city_name":"Mâcon","latitude":46.282944,"longitude":4.792796,"country_code":"FR"},{"city_name":"Lyon","latitude":45.746942,"longitude":4.835185,"country_code":"FR","arrival_place":true},{"city_name":"Aix-en-Provence","address":"Pasino GRAND","latitude":43.523155,"longitude":5.435842,"country_code":"FR"}],"bucketing_eligible":false,"booking_mode":"manual","booking_type":"online","is_booking_allowed":true,"view_count":166,"cross_border_alert":false,"trip_plan":[{"date":"18/10/2021 18:40:00","city":"Saint-Denis","address":"Hôpital Casanova","latitude":48.930632,"longitude":2.361332,"country_code":"FR","meeting_point_id":0,"vinci_shuttle_enabled":false},{"date":"18/10/2021 22:50:00","city":"Mâcon","address":"Péage de Mâcon Sud, 71000 Mâcon","latitude":46.282944,"longitude":4.792796,"country_code":"FR","meeting_point_id":0,"vinci_shuttle_enabled":false},{"date":"18/10/2021 23:40:00","city":"Lyon","address":"14 Avenue Berthelot, 69007 Lyon, France","latitude":45.746942,"longitude":4.835185,"country_code":"FR","meeting_point_id":342313,"vinci_shuttle_enabled":false},{"date":"19/10/2021 02:50:00","city":"Aix-en-Provence","address":"Pasino GRAND","latitude":43.523155,"longitude":5.435842,"country_code":"FR","meeting_point_id":0,"vinci_shuttle_enabled":false}],"messaging_status":"private","passengers":[],"display_contact":true,"vehicle_pictures":[],"can_report":false,"is_archived":false,"is_too_late_to_book":false,"is_booking_refused_by_driver":false,"has_already_booked_another_ride":false,"is_wrong_gender":false};

jest.mock("../../hooks/useTripData", () => ({
  useTripData: jest.fn()
}));

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
    }
  }
});

beforeEach(() => {
  useTripData.mockImplementation(() => ({
    data: mockTripData
  }));
});

it('should render correctly with received data', () => {
  const { getByText } = render(
    <QueryClientProvider client={queryClient}>
      <SearchResult tripID="2393829" />
    </QueryClientProvider>
  );

  expect(getByText(/from: saint-denis/i)).toBeDefined();
  expect(getByText(/(hôpital casanova)/i)).toBeDefined();
  expect(getByText(/to: lyon/i)).toBeDefined();
  expect(getByText(/(14 Avenue Berthelot, 69007 Lyon, France)/i)).toBeDefined();
  expect(getByText(/price: £22/i)).toBeDefined();
})

it('should render error message if no tripID is specified', () => {
  const { queryByText } = render(
    <QueryClientProvider client={queryClient}>
      <SearchResult />
    </QueryClientProvider>
  );

  expect(queryByText(/from: saint-denis/i)).toBeNull();
});