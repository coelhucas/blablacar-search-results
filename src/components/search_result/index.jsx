import React from "react";
import { useTripData } from "../../hooks/useTripData";
import "./styles.css";

const SearchResult = ({ tripID, ...props }) => {
  const { data, isLoading, error } = useTripData(tripID);

  if (isLoading) {
    return (
      <li>
        <p {...props}>Loading...</p>
      </li>
    );
  }

  if (!tripID) {
    return <p>Couldn't get tripID</p>
  }

  if (data?.error || !tripID) {
    return <p>{data.error}</p>;
  }

  if (error) {
    return (
      <p {...props}>
        Error: {error?.message}
      </p>
    );
  }

  const {
    departure_place: departure,
    price,
    arrival_place: arrival,
    links
  } = data;

  if (data.message) {
    return <li><p>⚠️ {data.message}</p></li>
  }
  
  return (
    <li>
      <div className="trip-container" {...props}>
        <a href={links._front} target="_blank" rel="noopener">
          <p>
            From: {departure.city_name} ({departure.address})
          </p>
          <p>
            To: {arrival.city_name} ({arrival.address})
          </p>
          <p>
            Price: {price.string_value}
          </p>
        </a>
      </div>
    </li>
  );
};

export default SearchResult;
