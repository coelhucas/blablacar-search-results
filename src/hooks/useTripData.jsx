import { useQuery } from "react-query";

const API_URL = "https://public-api.blablacar.com/api/v2/trips/";

/**
 * Fetches trip information from blablacar's API
 * 
 * @param {string} tripID - The ID of the trip to get data for
 * @returns {object} - Trip data: see https://support.blablacar.com/hc/en-gb/articles/360014264519--Trip-Details-API-Documentation
 */
const fetchTripById = async (tripID) => {
  if (tripID && typeof(tripID) !== "string") {
    throw new TypeError("tripID parameter must be a string, received: " + typeof(tripID));
  }

  const res = await fetch(`${API_URL}${tripID}?key=${process.env.REACT_APP_API_KEY}`);
  const data = await res.json();
  return data;
};

/**
 * Abstraction to handle fetching trip data from the API
 * 
 * @param {*} tripID - id of desired trip, e.g.: '2305114164-paris-lyon' 
 * @returns @type {UseQueryResult} - useQuery result: see https://react-query.tanstack.com/reference/useQuery
 */
export function useTripData(tripID) {
  return useQuery(["tripResult", tripID], () =>
    fetchTripById(tripID)
  );
}
