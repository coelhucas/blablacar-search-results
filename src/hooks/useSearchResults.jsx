import { useInfiniteQuery } from "react-query";

const API_URL =
  `https://public-api.blablacar.com/api/v3/trips?key=${process.env.REACT_APP_API_KEY}&from_coordinate=48.8566%2C2.3522&to_coordinate=45.764043%2C4.835659&count=5&from_country=FR&to_country=FR&locale=en-GB&start_date_local=2020-07-10T20:00:00&currency=EUR`;

const fetchResults = async (nextCursor = null) => {
  const nextCursorParam = nextCursor ? `&from_cursor=${nextCursor}` : "";
  const res = await fetch(`${API_URL}${nextCursorParam}`);
  const data = await res.json();
  return data;
};

/**
 * Abstraction to handle 'load more' capabilities for infinite scroll in search results
 * 
 * @returns {[{}, function]} - UseInfiniteQueryResult, see: https://react-query.tanstack.com/reference/useInfiniteQuery
 */
export function useSearchResults() {
  return useInfiniteQuery(
    "searchResults",
    ({ pageParam = null }) => fetchResults(pageParam),
    {
      getNextPageParam: (lastPage) => {
        return lastPage.next_cursor ?? false;
      }
    }
  );
}