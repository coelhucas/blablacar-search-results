import { QueryClient, QueryClientProvider } from "react-query";
import { renderHook } from '@testing-library/react-hooks';
import { useSearchResults } from "./useSearchResults";
import { useTripData } from "./useTripData";

function createWrapper() {
  const testQueryClient = new QueryClient({
    defaultOptions: {
      queries: {
        retry: false,
      }
    }
  })
  return ({ children }) => (
      <QueryClientProvider client={testQueryClient}>{children}</QueryClientProvider>
  )
}

describe('given useSearchResults hook', () => {
  it('should be initially in loading state', () => {
    const { result } = renderHook(() => useSearchResults(), {
      wrapper: createWrapper()
    });
    
    expect(result.current.data).not.toBeDefined();
    expect(result.current.isLoading).toBe(true);
  });

  it('should successfully return search results data', async () => {
    const { result, waitFor } = renderHook(() => useSearchResults(), {
      wrapper: createWrapper()
    });
    
    await waitFor(() => result.current.isSuccess);
    expect(result.current.data).toBeDefined()
  });
});

describe('given useTripData hook', () => {
  it('should be initially in loading state', () => {
    const { result } = renderHook(() => useTripData(), {
      wrapper: createWrapper()
    });
    
    expect(result.current.data).not.toBeDefined();
    expect(result.current.isLoading).toBe(true);
  });

  it('should successfully return search results data receiving "tripID" parameter', async () => {
    const { result, waitFor } = renderHook(() => useTripData('2305114164-paris-lyon'), {
      wrapper: createWrapper()
    });
    
    await waitFor(() => result.current.isSuccess);
    expect(result.current.data).toBeDefined()
  });

  it('should not accept any primitives different from string or undefined', async () => {
    /* We expect to have a console.error so this pass. Omitting just for better runner readability */
    jest.spyOn(console, 'error').mockImplementation(() => {});

    try {
      renderHook(() => useTripData(2305114164), {
        wrapper: createWrapper()
      });
    } catch(error) {
      expect(error.message).toBe(/tripid must be a string, received: number/i);
    }

  })
});