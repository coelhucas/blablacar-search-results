# BlaBlaCar Search Results Case

![image](https://user-images.githubusercontent.com/28108272/137765522-60a2ee91-f423-4a2b-b01e-283da079ce9a.png)

# Abstract
This is the project I made in the last few hours to BlaBlaCar technical front-end challenge. Below you can see informations on how to run this project locally and some rationale behind its conception. [Link to live demo at Heroku](https://blablacar-case.herokuapp.com/)[^1][^2]

[^1]: There was no big rationale for choosing Heroku. I stick with it because I already had its CLI and it made easier to quickly deploy this project.

[^2]: Be aware if I wasn't rate limited from the API 😅 

# Running the project locally
```bash
git clone git@gitlab.com:coelhucas/blablacar-search-results.git
cd blablacar-search-results
cp .env.sample .env
```
You'll have to feed [your very own API_KEY](https://support.blablacar.com/hc/en-gb/articles/360014200220--How-to-use-BlaBlaCar-search-API-) to `.env`, so you can run it by:
```bash
yarn && yarn start
```

# Technical Decisions
Disclaimer: After I got rate limited by the API I wasn't able to re-test anything. Hopefully I did not broke anything just after sending the project.

## CodeSandbox
Most of the project was written inside codesandbox for simplicity. Close to the end I decided to download it and start working locally on my machine to get better debugging.

## Linting
Something that I'd do if I had a few more time would be adding a linter + prettier to auto-format the code. Since I used CodeSandbox, it came with some ESLint configuration but after moving to a local repo I stopped using it since it lacks the default integration that CodeSandbox have.

## Git
Due to most of the project being made inside CodeSandbox, I didn't had a git history, but since it wasn't a requirement inside the case, I think it's okay

## API Key
I exposed the API keys in this project at first. Usually I'd either cycle it or use some utility such as [git-filter-repo](https://github.com/newren/git-filter-repo) to remove it from my git history, but due to this project scope I deliberately decided to keep this way, but aware of this being a flaw.

## Load more
I made the API return 5 results in count so I could show how it is working in the live demo. `react-query`'s `useInfiniteQuery` made this work easier than ever and it was a quick feature to implement.

## Styles
I took it to the extreme the specifications of the case, which made the interface look terrible. Hope it's not a problem 😅

## Typing
I wanted to do this entirely on TypeScript, but since I wasn't able to have the entirity of the time available for this challenge, I decided to go without any sulution such as TypeScript, PropTypes or flow. I understand this can lead the project to some unpredicted bugs and issues, but I did my best to attempt to ensure everything would work the best way possible.

## Error handling
This was the biggest flaw during the development. I tried to do my best to make it decent but due to time I didn't prioritize this part of the code and visual feedbacks due to the scope of the project, but I'm well aware of this issue.

## Tests
![image](https://user-images.githubusercontent.com/28108272/137773455-c4ca3ef2-a98a-43fc-9bda-5bb0e1ccf3e7.png)

In a real project and with more time, I'd add snapshot tests and if we could, E2E using some tool such as Cypress.

### Components
I did basic specs for main components here, `<SearchResultList />` and `<SearchResult />`, it checked most rendering of received data to work as unit regression tests like if it was a project that would be maintained.

### Hooks
Also did tests for the custom hooks made to make cleaner usage of `react-query`. Since it was my first time with the lib it I'm not sure if I did the best possible, but I used my previous skills with hook testing to ensure it was decent.

## Depedencies

### Testing library(react|react-hooks)
I decided to stick with those, which I'm already used to work with and are just fine for what I needed.

### React Query
I decided to be audacious on this, since I see lots of people talking about it and I never used it yet. But I think it was a awesome experience for a starter with this library. It leverages some concerns such as caching and internal logic of errors and loading states.
